libgav1 (0.19.0-3+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Wed, 26 Feb 2025 14:19:54 +0000

libgav1 (0.19.0-3) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Add architecture-is-little-endian to build dependencies.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on cmake (>= 3.7.1).

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 29 Dec 2024 13:58:32 +0100

libgav1 (0.19.0-2) unstable; urgency=medium

  * Team upload
  * Upload to unstable (Closes: #1068180)
  * debian/patches: Temporarily disable cpu test

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 01 Apr 2024 15:14:00 +0200

libgav1 (0.19.0-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/patches/0001-Unbundle-abseil.patch: Refresh the patch.
  * debian/patches/0002-Use-system-googletest.patch: Try to enable
    googletest.
  * debian/patches/0003-Use-C-14.patch: Force C++14 standard to avoid
    FTBFS with googletest.

 -- Boyuan Yang <byang@debian.org>  Mon, 27 Nov 2023 23:17:46 -0500

libgav1 (0.18.0-1+apertis2) apertis; urgency=medium

  * Bump changelog to trigger license scan report

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Thu, 27 Jul 2023 14:17:34 +0530

libgav1 (0.18.0-1+apertis1) apertis; urgency=medium

  * Add debian/apertis/copyright

 -- Vignesh Raman <vignesh.raman@collabora.com>  Thu, 13 Apr 2023 19:35:32 +0530

libgav1 (0.18.0-1+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to target.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 12 Apr 2023 19:54:24 +0530

libgav1 (0.18.0-1) unstable; urgency=medium

  * Upload to unstable.
  * debian/rules: Properly use pkgkde_symbolshelper dh plugin.
  * debian/control: Add build-dependency on pkg-kde-tools.

 -- Boyuan Yang <byang@debian.org>  Wed, 27 Jul 2022 16:50:15 -0400

libgav1 (0.18.0-1~exp2) experimental; urgency=medium

  * debian/rules: Enable lto build.

 -- Boyuan Yang <byang@debian.org>  Wed, 27 Jul 2022 12:32:14 -0400

libgav1 (0.18.0-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream version 0.18.0. (Closes: #1014934)
  * Refresh patches.
  * debian/control: Bump Standards-Version to 4.6.1.
  * Bump SONAME: 0 -> 1, with library package rename.
  * debian/libgav1-1.symbols: Reset symbols.

 -- Boyuan Yang <byang@debian.org>  Sat, 16 Jul 2022 14:10:20 -0400

libgav1 (0.17.0-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation lines.

  [ Boyuan Yang ]
  * New upstream release 0.17.0. 

 -- Boyuan Yang <byang@debian.org>  Sun, 07 Nov 2021 08:53:56 -0500

libgav1 (0.16.3-2) unstable; urgency=medium

  * Team upload.
  * Upload to sid.

  [ qinxialei ]
  * d/copyright:
    + Update copyright

 -- Boyuan Yang <byang@debian.org>  Sun, 15 Aug 2021 13:09:06 -0400

libgav1 (0.16.3-1~exp1) experimental; urgency=medium

  * New upstream version 0.16.3
  * d/control:
    + Bump Standards-Version to 4.5.1.
  * d/patches:
    + Update patch:Unbundling-abseil.patch
  * d/copyright:
    + Update copyright

 -- qinxialei <xialeiqin@gmail.com>  Thu, 22 Apr 2021 10:35:22 +0800

libgav1 (0.16.0-5) unstable; urgency=medium

  * Team upload.
  * debian/libgav1-0.symbols: Refresh symbols again.

 -- Boyuan Yang <byang@debian.org>  Sun, 08 Nov 2020 20:38:23 -0500

libgav1 (0.16.0-5) unstable; urgency=medium

  * Team upload.
  * debian/libgav1-0.symbols: Refresh symbols again.

 -- Boyuan Yang <byang@debian.org>  Sun, 08 Nov 2020 20:38:23 -0500

libgav1 (0.16.0-4) unstable; urgency=medium

  * Team upload.
  * debian/libgav1-0.symbols: Try to distinguish by symbol bits.
  * debian/rules: Also explicitly disable AVX2 per baseline.

 -- Boyuan Yang <byang@debian.org>  Sat, 31 Oct 2020 12:26:14 -0400

libgav1 (0.16.0-3) unstable; urgency=medium

  * Team upload.
  * debian/libgav1-0.symbols: refresh symbols.
  * debian/rules: Adjust build flags to meet Debian's ISA baseline
    requirement.

 -- Boyuan Yang <byang@debian.org>  Fri, 30 Oct 2020 23:02:37 -0400

libgav1 (0.16.0-2) unstable; urgency=medium

  * Team upload.
  * debian/control: Update package description to remove mentions
    of advanced build flags since we are not enabling them.

 -- Boyuan Yang <byang@debian.org>  Thu, 29 Oct 2020 10:19:44 -0400

libgav1 (0.16.0-1) unstable; urgency=medium

  * Initial release (Closes: #973288)

 -- qinxialei <xialeiqin@gmail.com>  Wed, 28 Oct 2020 17:35:33 +0800
